# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in caa_web/__init__.py
from caa_web import __version__ as version

setup(
	name='caa_web',
	version=version,
	description='caa',
	author='caa',
	author_email='caa@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
