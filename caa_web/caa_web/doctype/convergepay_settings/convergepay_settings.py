# -*- coding: utf-8 -*-
# Copyright (c) 2021, caa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.integrations.utils import create_request_log, create_payment_gateway
from frappe.utils import get_url, call_hook_method, cint, flt, cstr
from six.moves.urllib.parse import urlencode
class ConvergepaySettings(Document):
    supported_currencies = ["INR", "CAD"]

    def validate(self):
        create_payment_gateway('Convergepay')

    def validate_transaction_currency(self, currency):
        if currency not in self.supported_currencies:
            frappe.throw(
                _("Please select another payment method. converge does not support transactions in currency '{0}'").format(currency))

    def get_payment_url(self, **kwargs):    
        try:
            integration_request = create_request_log(kwargs, "Host", "Convergepay")
            amou=kwargs['amount']
            description=kwargs['description']
            desc=str(description)
            if 'SAL' in desc: 
                st=desc.index('SAL')
                salid=desc[st::]
                print(salid)
                payreq=kwargs['order_id']
                return get_url("/payconvergepage?amount={0}&transalesid={1}&payr={2}".format(amou,salid,payreq))
        except:
            pass
