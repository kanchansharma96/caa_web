from __future__ import unicode_literals, print_function
import frappe
import json
import requests
from erpnext.selling.doctype.sales_order.sales_order import make_sales_invoice

VALID_TRANSACTION_TYPES = [
    'ccauthonly',
    'ccavsonly',
    'ccsale',
    'ccverify',
    'ccgettoken',
    'cccredit',
    'ccforce',
    'ccbalinquiry',
    'ccgettoken',
    'ccreturn',
    'ccvoid',
    'cccomplete',
    'ccdelete',
    'ccupdatetip',
    'ccsignature',
    'ccaddrecurring',
    'ccaddinstall',
    'ccupdatetoken',
    'ccdeletetoken',
    'ccquerytoken'
]


class ConvergenceException(Exception):
    pass


class Converge(object):
    def __init__(self, merchant_id, user_id, pin, is_demo=False):
        self._merchant_id = merchant_id
        self._user_id = user_id
        self._pin = pin
        self._is_demo = is_demo

    @property
    def xml_endpoint(self):
        if self._is_demo:
            return "https://api.demo.convergepay.com/VirtualMerchantDemo/process.do"
        return "https://api.convergepay.com/VirtualMerchant/process.do"

    def request(self, transaction_type, **kwargs):
        """
        Make request to Converge with transaction type and parameters.
        :param transaction_type: a valid transaction type that's in VALID_TRANSACTION_TYPES
        :param kwargs:
        :return:
        """
        if transaction_type not in VALID_TRANSACTION_TYPES:
            raise ConvergenceException(
                '{} is not a valid transaction type'.format(transaction_type))

        kwargs['ssl_transaction_type'] = transaction_type
        return self._http_request(**kwargs)

    def _http_request(self, **kwargs):
        kwargs['ssl_merchant_id'] = self._merchant_id
        kwargs['ssl_user_id'] = self._user_id
        kwargs['ssl_pin'] = self._pin
        kwargs['ssl_show_form'] = 'false'
        kwargs['ssl_result_format'] = 'ascii'

        response = requests.post(self.xml_endpoint, kwargs)
        response.raise_for_status()
        result = {}
        decoded = response.content.decode('utf-8')
        for line in decoded.split('\n'):
            k, v = line.split('=')
            result[k] = v
        return result


@frappe.whitelist(allow_guest=True)
def converge_pay(cardNumber=None, expiryMonth=None,  cvvCode=None, amount=None, sales_invoice_name=None, sales_order_name=None, sales_order_payreq=None):
    merchant_id = frappe.get_doc("Convergepay Settings").merchant_id
    user_id = frappe.get_doc("Convergepay Settings").user_id
    pin = frappe.get_doc("Convergepay Settings").pin

    convergepayval = Converge(merchant_id, user_id, pin, is_demo=True)
    details = convergepayval.request('ccsale',  # ccsale,ccverify
                                     ssl_card_number=cardNumber,
                                     ssl_exp_date=str(expiryMonth),
                                     ssl_cvv2cvc2=cvvCode,
                                     ssl_amount=amount)
    try:
        if details['ssl_result_message'] == 'APPROVAL':
            if 'SINV' in sales_invoice_name:
                payment_request_name = frappe.db.sql( """select name from `tabPayment Request` where reference_name="{}" """.format(sales_invoice_name), as_dict=1)
                payment_request_id = payment_request_name[0].name
                sales_invoice_data = frappe.db.sql( """select * from `tabSales Invoice` where name="{}" """.format(sales_invoice_name), as_dict=1)
                payment_entry_by_sales_invoice = frappe.get_doc({
                    "doctype": "Payment Entry",
                    "payment_type": 'Receive',
                    "company": sales_invoice_data[0].company,
                    "party_type": 'Customer',
                    "mode_of_payment": 'Credit Card',
                    "party": sales_invoice_data[0].customer,
                    "paid_amount": float(sales_invoice_data[0].grand_total),
                    "received_amount": float(sales_invoice_data[0].grand_total),
                    "reference_no": payment_request_id, 
                    "reference_date": sales_invoice_data[0].posting_date,
                    "paid_to": "5221398 - TD Canada Trust (TD Bank) - CAA",
                    "references": [{
                        "reference_doctype": "Sales Invoice",
                        "reference_name": sales_invoice_data[0].name,
                        "allocated_amount":float(sales_invoice_data[0].grand_total)
                    }]
                })
                payment_entry_by_sales_invoice.save(ignore_permissions=True)
                payment_entry_by_sales_invoice.submit()

                return "APPROVAL"
            else:
                salesorderdetail = frappe.db.sql(""" select * from `tabSales Order` where name="{}" """.format(sales_order_name), as_dict=1)
                sales_invoice = make_sales_invoice(sales_order_name, ignore_permissions=True)
                sales_invoice = sales_invoice.insert(ignore_permissions=True)
                sales_invoice.submit()
                payment_entry_by_sales_invoices = frappe.get_doc({
                    "doctype": "Payment Entry",
                    "payment_type": 'Receive',
                    "company": salesorderdetail[0].company,
                    "party_type": 'Customer',
                    "mode_of_payment": 'Credit Card',
                    "party": salesorderdetail[0].customer,
                    "paid_amount": float(salesorderdetail[0].grand_total),
                    "received_amount": float(salesorderdetail[0].grand_total),
                    "reference_no": sales_order_payreq,  # paymentrequestid
                    "reference_date": salesorderdetail[0].transaction_date,
                    "paid_to": "5221398 - TD Canada Trust (TD Bank) - CAA",
                    "references": [{
                        "reference_doctype": "Sales Invoice",
                        "reference_name": sales_invoice.name,  # salesorder.name
                        "allocated_amount": float(salesorderdetail[0].grand_total)
                    }]
                })
                payment_entry_by_sales_invoices.save(ignore_permissions=True)
                payment_entry_by_sales_invoices.submit()
                return "APPROVAL"
    except:
        if details['errorName'] == "Credit Card Number Invalid":
            return "card"
        elif details['errorName'] == "Exp Date Invalid":
            return "expiry"
        elif details['errorName'] == "Invalid CVV2 Value":
            return "cvv"
        else:
            pass
